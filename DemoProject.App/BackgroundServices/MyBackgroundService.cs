﻿using DemoProject.BusinessLogicLayer.Services.Authors;

namespace DemoProject.App.BackgroundServices;

public class MyBackgroundService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    
    public MyBackgroundService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await Task.Delay(1, stoppingToken); // Для запуска следующего сервиса. Потом можешь вопрос задать, поясню суть
        
        using var scope = _serviceProvider.CreateScope();
        var authorService = scope.ServiceProvider.GetRequiredService<IAuthorService>();
        var logger = scope.ServiceProvider.GetRequiredService<ILogger<MyBackgroundService>>();
        while (!stoppingToken.IsCancellationRequested)
        {
            var text = authorService.BackgroundServiceMethod();
            logger.LogError("Текст из метода: {Text}", text);
            
            await Task.Delay(5000, stoppingToken);
        }
    }
}