﻿using DemoProject.App.BackgroundServices;
using DemoProject.BusinessLogicLayer.ServicesConfigurations;

namespace DemoProject.App.Configurations;

public static class ServicesConfiguration
{
    /// <summary>
    /// Подключение основных классов
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.BusinessLogicLayerServicesConfigure(configuration);
        services.AddHostedService<MyBackgroundService>();
    }

    /// <summary>
    /// Конфигурация приложения
    /// </summary>
    /// <param name="serviceProvider"></param>
    public static void ConfigureApp(this IServiceProvider serviceProvider)
    {
        serviceProvider.BusinessLogicLayerConfigureApp();
    }
}