using DemoProject.App.Models;
using DemoProject.BusinessLogicLayer.Services.Authors;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.App.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        /// <summary>
        /// Получить список авторов
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public Task<IEnumerable<AuthorDto>> Get(CancellationToken cancellationToken = default)
        {
            return _authorService.GetAuthors(cancellationToken);
        }
        
        /// <summary>
        /// Получить указанного автора
        /// </summary>
        /// <param name="authorId">Ид автора</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("{authorId}")]
        public Task<AuthorDto> Get(Guid authorId, CancellationToken cancellationToken = default)
        {
            return _authorService.GetAuthor(authorId, cancellationToken);
        }
        
        /// <summary>
        /// Добавить автора
        /// </summary>
        /// <param name="authorCreateModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<Guid> Add(AuthorCreateModel authorCreateModel, CancellationToken cancellationToken = default)
        {
            // Тут можно было заюзать automapper и смапить AuthorCreateModel в AuthorDto
            return _authorService.CreateAuthor(new AuthorDto()
            {
                AuthorName = authorCreateModel.AuthorName
            }, cancellationToken);
        }
        
        /// <summary>
        /// Изменить автора
        /// </summary>
        /// <param name="authorEditModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut]
        public Task Edit(AuthorEditModel authorEditModel, CancellationToken cancellationToken = default)
        {
            // Тут можно было заюзать automapper и смапить AuthorEditModel в AuthorDto
            return _authorService.UpdateAuthor(new AuthorDto()
            {
                Id = authorEditModel.Id,
                AuthorName = authorEditModel.AuthorName
            }, cancellationToken);
        }
        
        /// <summary>
        /// Удалить автора
        /// </summary>
        /// <param name="authorId">Ид автора</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete("{authorId}")]
        public Task Delete(Guid authorId, CancellationToken cancellationToken = default)
        {
            return _authorService.DeleteAuthor(authorId, cancellationToken);
        }
    }
}