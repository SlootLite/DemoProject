﻿using System.ComponentModel.DataAnnotations;

namespace DemoProject.App.Models;

public class AuthorEditModel
{
    /// <summary>
    /// Имя автора
    /// </summary>
    [Required]
    public Guid Id { get; set; }
    
    /// <summary>
    /// Имя автора
    /// </summary>
    [Required]
    public string AuthorName { get; set; }
}