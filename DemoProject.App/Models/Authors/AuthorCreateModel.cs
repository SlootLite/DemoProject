﻿using System.ComponentModel.DataAnnotations;

namespace DemoProject.App.Models;

public class AuthorCreateModel
{
    /// <summary>
    /// Имя автора
    /// </summary>
    [Required]
    public string AuthorName { get; set; }
}