﻿using DemoProject.BusinessLogicLayer.MappingConfigurations;
using DemoProject.BusinessLogicLayer.Services.Authors;
using DemoProject.DataAccessLayer.ServicesConfigurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DemoProject.BusinessLogicLayer.ServicesConfigurations;

public static class BusinessLogicLayerServicesConfiguration
{
    /// <summary>
    /// Подключение классов в BusinessLogicLayer
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    public static void BusinessLogicLayerServicesConfigure(this IServiceCollection services, IConfiguration configuration)
    {
        services.DataAccessLayerServicesConfigure(configuration);
        services.ConfigureMapping();
        services.AddScoped<IAuthorService, AuthorService>();
    }
    
    /// <summary>
    /// Конфигурация приложения
    /// </summary>
    /// <param name="serviceProvider"></param>
    public static void BusinessLogicLayerConfigureApp(this IServiceProvider serviceProvider)
    {
        serviceProvider.DataAccessLayerConfigureApp();
    }
}