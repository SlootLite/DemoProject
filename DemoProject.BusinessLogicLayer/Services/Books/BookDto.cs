﻿using DemoProject.BusinessLogicLayer.Services.Authors;

namespace DemoProject.BusinessLogicLayer.Services.Books;

public class BookDto
{
    /// <summary>
    /// Ид книги
    /// </summary>
    public Guid Id { get; set; }
    
    /// <summary>
    /// Ид автора
    /// </summary>
    public Guid AuthorId { get; set; }
    
    /// <summary>
    /// Автор
    /// </summary>
    public AuthorDto Author { get; set; }
    
    /// <summary>
    /// Название книги
    /// </summary>
    public string BookName { get; set; }
}