﻿using DemoProject.BusinessLogicLayer.Services.Books;

namespace DemoProject.BusinessLogicLayer.Services.Authors;

public class AuthorDto
{
    /// <summary>
    /// Ид автора книги
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Имя автора
    /// </summary>
    public string AuthorName { get; set; }
    
    /// <summary>
    /// Книги автора
    /// </summary>
    public IEnumerable<BookDto> Books { get; set; }
}