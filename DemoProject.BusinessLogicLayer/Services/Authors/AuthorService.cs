﻿using AutoMapper;
using DemoProject.DataAccessLayer;
using DemoProject.DataAccessLayer.Entities;
using DemoProject.DataAccessLayer.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.BusinessLogicLayer.Services.Authors;

public interface IAuthorService
{
    /// <summary>
    /// Получить список авторов
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<AuthorDto>> GetAuthors(CancellationToken cancellationToken);

    /// <summary>
    /// Получить автора
    /// </summary>
    /// <param name="id">Ид автора</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<AuthorDto> GetAuthor(Guid id, CancellationToken cancellationToken);

    /// <summary>
    /// Создать автора
    /// </summary>
    /// <param name="authorDto"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Guid> CreateAuthor(AuthorDto authorDto, CancellationToken cancellationToken);

    /// <summary>
    /// Изменить автора
    /// </summary>
    /// <param name="authorDto"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task UpdateAuthor(AuthorDto authorDto, CancellationToken cancellationToken);

    /// <summary>
    /// Удалить автора
    /// </summary>
    /// <param name="authorId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task DeleteAuthor(Guid authorId, CancellationToken cancellationToken);

    /// <summary>
    /// Просто метод для фонового сервиса для примера
    /// </summary>
    /// <returns></returns>
    string BackgroundServiceMethod();
}

public class AuthorService : IAuthorService
{
    private readonly MainContext _mainContext;
    private readonly IMapper _mapper;
    private const string AuthorNotFoundText = "Указанный автор не найден";
    
    public AuthorService(MainContext mainContext, 
        IMapper mapper)
    {
        _mainContext = mainContext;
        _mapper = mapper;
    }

    public async Task<IEnumerable<AuthorDto>> GetAuthors(CancellationToken cancellationToken)
    {
        var authors = await _mainContext.Authors.AsNoTracking().ToListAsync(cancellationToken);
        
        return _mapper.Map<IEnumerable<AuthorDto>>(authors);
    }

    public async Task<AuthorDto> GetAuthor(Guid id, CancellationToken cancellationToken)
    {
        var author = await _mainContext.Authors.AsNoTracking()
            .Include(s => s.Books)
            .FirstOrDefaultAsync(s => s.Id == id, cancellationToken);
        
        return _mapper.Map<AuthorDto>(author);
    }

    public async Task<Guid> CreateAuthor(AuthorDto authorDto, CancellationToken cancellationToken)
    {
        var authorId = Guid.NewGuid();
        
        await _mainContext.Authors.AddAsync(new AuthorEntity()
        {
            Id = authorId,
            AuthorName = authorDto.AuthorName
        }, cancellationToken);
        await _mainContext.SaveChangesAsync(cancellationToken);
        
        return authorId;
    }

    public async Task UpdateAuthor(AuthorDto authorDto, CancellationToken cancellationToken)
    {
        var author = await _mainContext.Authors.FirstOrDefaultAsync(s => s.Id == authorDto.Id, cancellationToken);
        
        if (author == null)
        {
            throw new DataNotFoundException(AuthorNotFoundText);
        }

        author.AuthorName = authorDto.AuthorName;

        await _mainContext.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteAuthor(Guid authorId, CancellationToken cancellationToken)
    {
        var author = await _mainContext.Authors.FirstOrDefaultAsync(s => s.Id == authorId, cancellationToken);
        
        if (author == null)
        {
            throw new DataNotFoundException(AuthorNotFoundText);
        }
        
        _mainContext.Authors.Remove(author);
        
        await _mainContext.SaveChangesAsync(cancellationToken);
    }

    public string BackgroundServiceMethod()
    {
        return "Text";
    }
}