﻿using AutoMapper;
using DemoProject.BusinessLogicLayer.Services.Books;
using DemoProject.DataAccessLayer.Entities;

namespace DemoProject.BusinessLogicLayer.MappingConfigurations.BookProfiles;

public class BookProfile : Profile
{
    public BookProfile()
    {
        CreateMap<BookEntity, BookDto>().ReverseMap();
    }
}