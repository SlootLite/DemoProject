﻿using AutoMapper;
using DemoProject.BusinessLogicLayer.Services.Authors;
using DemoProject.DataAccessLayer.Entities;

namespace DemoProject.BusinessLogicLayer.MappingConfigurations.AuthorProfiles;

public class AuthorProfile : Profile
{
    public AuthorProfile()
    {
        CreateMap<AuthorEntity, AuthorDto>().ReverseMap();
    }
}