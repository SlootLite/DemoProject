﻿using AutoMapper;
using DemoProject.BusinessLogicLayer.MappingConfigurations.AuthorProfiles;
using DemoProject.BusinessLogicLayer.MappingConfigurations.BookProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace DemoProject.BusinessLogicLayer.MappingConfigurations;

public static class MappingConfiguration
{
    /// <summary>
    /// Подключение конфигураций автомаппера
    /// </summary>
    /// <param name="services"></param>
    public static void ConfigureMapping(this IServiceCollection services)
    {
        services.AddSingleton(provider => new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<AuthorProfile>();
            cfg.AddProfile<BookProfile>();
        }).CreateMapper());
    }
}