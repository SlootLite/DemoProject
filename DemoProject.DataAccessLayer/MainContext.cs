﻿using System.Reflection;
using DemoProject.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.DataAccessLayer;

public class MainContext : DbContext
{
    public MainContext(DbContextOptions<MainContext> options) : base(options)
    {
    }
    
    /// <summary>
    /// Книги
    /// </summary>
    public DbSet<BookEntity> Books { get; set; }
    
    /// <summary>
    /// Авторы книг
    /// </summary>
    public DbSet<AuthorEntity> Authors { get; set; }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}