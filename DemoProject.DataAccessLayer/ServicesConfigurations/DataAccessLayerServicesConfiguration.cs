﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DemoProject.DataAccessLayer.ServicesConfigurations;

public static class DataAccessLayerServicesConfiguration
{
    /// <summary>
    /// Подключение классов в DataAccessLayer
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    public static void DataAccessLayerServicesConfigure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<MainContext>(c =>
            c.UseSqlServer(configuration.GetConnectionString("MainConnection")));
    }

    /// <summary>
    /// Конфигурация приложения
    /// </summary>
    /// <param name="serviceProvider"></param>
    public static void DataAccessLayerConfigureApp(this IServiceProvider serviceProvider)
    {
        serviceProvider.Migrate();
    }
    
    /// <summary>
    /// Миграции
    /// </summary>
    /// <param name="serviceProvider"></param>
    private static void Migrate(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<MainContext>();
        context.Database.Migrate();
    }
}