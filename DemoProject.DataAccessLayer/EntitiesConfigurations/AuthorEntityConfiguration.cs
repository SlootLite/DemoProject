﻿using DemoProject.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoProject.DataAccessLayer.EntitiesConfigurations;

public class AuthorEntityConfiguration : IEntityTypeConfiguration<AuthorEntity>
{
    public void Configure(EntityTypeBuilder<AuthorEntity> builder)
    {
        builder.ToTable("Authors");
        
        builder.Property(s => s.AuthorName)
            .IsRequired()
            .HasColumnType("varchar(300)");
    }
}