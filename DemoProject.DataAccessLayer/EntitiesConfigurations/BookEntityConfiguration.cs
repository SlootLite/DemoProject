﻿using DemoProject.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoProject.DataAccessLayer.EntitiesConfigurations;

public class BookEntityConfiguration : IEntityTypeConfiguration<BookEntity>
{
    public void Configure(EntityTypeBuilder<BookEntity> builder)
    {
        builder.ToTable("Books");
        
        builder.Property(s => s.BookName)
            .IsRequired()
            .HasColumnType("varchar(300)");
        
        builder.HasOne(s => s.Author)
            .WithMany(s => s.Books)
            .OnDelete(DeleteBehavior.Cascade);
    }
}