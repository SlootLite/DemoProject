﻿namespace DemoProject.DataAccessLayer.Entities;

public class AuthorEntity
{
    /// <summary>
    /// Ид автора книги
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Имя автора
    /// </summary>
    public string AuthorName { get; set; }
    
    /// <summary>
    /// Книги автора
    /// </summary>
    public IEnumerable<BookEntity> Books { get; set; }
}