﻿namespace DemoProject.DataAccessLayer.Entities;

public class BookEntity
{
    /// <summary>
    /// Ид книги
    /// </summary>
    public Guid Id { get; set; }
    
    /// <summary>
    /// Ид автора
    /// </summary>
    public Guid AuthorId { get; set; }
    
    /// <summary>
    /// Автор
    /// </summary>
    public AuthorEntity Author { get; set; }
    
    /// <summary>
    /// Название книги
    /// </summary>
    public string BookName { get; set; }
}