﻿namespace DemoProject.DataAccessLayer.Exceptions;

/// <summary>
/// Исключение. Данные не найдены
/// </summary>
public class DataNotFoundException : Exception
{
    public DataNotFoundException()
    {
        
    }

    public DataNotFoundException(string message) : base(message)
    {
        
    }
}